package main

import (
	"context"
	"fmt"
	"github.com/chromedp/cdproto/cdp"
	"log"
	"regexp"
	"strings"
	"unicode"

	"github.com/chromedp/chromedp"
)

func main() {
	url := "https://fanated.com/"

	ctxNav, cancel := chromedp.NewContext(context.Background())
	defer cancel()

	var links []*cdp.Node
	err := chromedp.Run(ctxNav,
		chromedp.Navigate(url),
		chromedp.WaitVisible("body", chromedp.ByQuery),
		chromedp.Nodes("a", &links),
	)
	if err != nil {
		log.Fatal("Failed to parse:", err)
	}
	mp := make(map[string]int)
	for _, link := range links {
		hrefValue, _ := link.Attribute("href")
		if len(hrefValue) > 1 {
			matched, _ := regexp.MatchString("css", hrefValue)
			if !matched {
				mp[hrefValue]++
			}
		}
	}

	for k := range mp {
		url = "https://fanated.com/" + k
		fmt.Println(url)
		var res string
		err = chromedp.Run(ctxNav,
			chromedp.Navigate(url),
			chromedp.Text("body", &res),
		)
		ans := Checker(res)
		for _, words := range ans {
			fmt.Println(words)
		}
	}
}

func Checker(text string) []string {
	var res []string
	text = strings.ToLower(text)
	words := strings.Split(text, " ")
	for _, word := range words {
		for _, char := range word {
			if unicode.Is(unicode.Cyrillic, char) {
				res = append(res, word)
			}
		}
	}
	return res
}
