package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"golang.org/x/net/context"
	"golang.org/x/oauth2/google"
	"log"
	"os"

	"google.golang.org/api/sheets/v4"
)

func main() {
	err := godotenv.Load("../config/.env")
	if err != nil {
		log.Fatalf("Error loading .env file")
	}
	url := os.Getenv("URL")
	spreadsheetId := os.Getenv("SpreadSheetId")
	fmt.Println(url, spreadsheetId)
	ctx := context.Background()

	b, err := os.ReadFile("../config/credentials.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}
	config, err := google.ConfigFromJSON(b, "https://www.googleapis.com/auth/spreadsheets")
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
	}
	c := getClient(config)

	sheetsService, err := sheets.New(c)
	if err != nil {
		log.Fatal(err)
	}

	range2 := "A1:A"
	valueInputOption := "USER_ENTERED"
	insertDataOption := "INSERT_ROWS"

	header := []interface{}{"Адрес страницы", "название страницы", "описание страницы", "тэги", "наличие русских символов"}

	var rbData [][]interface{}
	rbData = append(rbData, header)

	data := Result(url)
	rbData = append(rbData, data...)
	rb := &sheets.ValueRange{
		Values: rbData,
	}
	resp, err := sheetsService.Spreadsheets.Values.Append(spreadsheetId, range2, rb).ValueInputOption(valueInputOption).InsertDataOption(insertDataOption).Context(ctx).Do()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%#v\n", resp)

}
