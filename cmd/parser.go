package main

import (
	"context"
	"fmt"
	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/chromedp"
	"log"
	"regexp"
	"strings"
	"unicode"
)

var (
	m = make(map[string]struct{})
)

func Result(URL string) [][]interface{} {

	ctx, cancel := chromedp.NewContext(context.Background())
	defer cancel()

	if err := chromedp.Run(ctx, chromedp.Navigate(URL)); err != nil {
		log.Fatal("Failed to navigate:", err)
	}
	CollectLinks(ctx, URL)

	var ans [][]interface{}
	for url := range m {
		var title string
		var description string
		var keyword string

		if err := chromedp.Run(ctx,
			chromedp.Navigate(url),
			chromedp.Title(&title)); err != nil {
			fmt.Println("Failed to get title:", title)
			title = ""
		}

		if err := chromedp.Run(ctx,
			chromedp.Navigate(url),
			chromedp.Evaluate(`document.querySelector('meta[name="description"]') !== null ? document.querySelector('meta[name="description"]').getAttribute('content') : ''`, &description)); err != nil {
			fmt.Println("Failed to get page description:", err)
			description = ""
		}

		if err := chromedp.Run(ctx,
			chromedp.Navigate(url),
			chromedp.Evaluate(`document.querySelector('meta[name="keywords"]') !== null ? document.querySelector('meta[name="keywords"]').getAttribute('content') : ''`, &keyword)); err != nil {
			fmt.Println("Failed to get page tags:", err)
			keyword = ""
		}

		var text string
		if err := chromedp.Run(ctx,
			chromedp.Navigate(url),
			chromedp.Text("body", &text),
		); err != nil {
			fmt.Println("Failed to get text:", err)
		}
		words := Check(text)
		ans = append(ans, []interface{}{url, title, description, keyword, strings.Join(words, ", ")})
	}
	return ans
}

func CollectLinks(ctx context.Context, url string) {
	var links []*cdp.Node
	err := chromedp.Run(ctx,
		chromedp.Navigate(url),
		chromedp.WaitVisible("body", chromedp.ByQuery),
		chromedp.Nodes("a", &links),
	)
	if err != nil {
		fmt.Println("Failed to parse:", err)
		return
	}
	mp := make(map[string]int)
	for _, link := range links {
		hrefValue, _ := link.Attribute("href")
		if len(hrefValue) > 1 {
			matched, _ := regexp.MatchString("css|img|fonts|vk", hrefValue)
			if !matched {
				mp[hrefValue]++
			}
		}
	}

	for k := range mp {
		var urlNew string
		matched, _ := regexp.MatchString(k, url)
		if matched {
			continue
		}
		matched, _ = regexp.MatchString("http", k)
		if matched {
			continue
		} else {
			urlNew = "https://fanated.com/" + k
		}
		matched, _ = regexp.MatchString("#", k)
		if matched {
			urlNew = url + "/" + k
		}
		if _, ok := m[urlNew]; ok {
			continue
		}
		m[urlNew] = struct{}{}
		fmt.Println(urlNew)
		CollectLinks(ctx, urlNew)
	}
}
func Check(text string) []string {
	var res []string
	text = strings.ToLower(text)
	words := strings.Split(text, " ")
	for _, word := range words {
		for _, char := range word {
			if unicode.Is(unicode.Cyrillic, char) {
				res = append(res, word)
			}
		}
	}
	return res
}
